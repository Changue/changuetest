﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace LearnTest1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

      
        private void button4_Click(object sender, EventArgs e)
        {     
        }

        string dirPath = Application.StartupPath + @"\\test";


        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();

           
            if (Directory.Exists(dirPath))
            {
                DirectoryInfo di = new DirectoryInfo(dirPath);
                foreach (var item in di.GetFiles())
                {
                    listBox1.Items.Add(item.Name);
                }
            }

        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
              
                pictureBox1.Image = Image.FromFile(dirPath + "/" + listBox1.SelectedItem.ToString());
                pictureBox1.Size = new Size(pictureBox1.Image.Size.Width, pictureBox1.Image.Size.Height);

                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

                panel1.Controls.Add(pictureBox1);

                //MessageBox.Show(listBox1.SelectedItem.ToString());
            }
            
        }

        float standtemp = 1.0f;
        private void button2_Click(object sender, EventArgs e)
        {
            standtemp -= 0.1f;
            float tempsize = pictureBox1.Image.Size.Width * standtemp;
            float tempsize2 = pictureBox1.Image.Size.Height * standtemp;
            pictureBox1.Size = new Size((int)tempsize, (int)tempsize2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            standtemp += 0.1f;
            float tempsize = pictureBox1.Image.Size.Width * standtemp;
            float tempsize2 = pictureBox1.Image.Size.Height * standtemp;
            pictureBox1.Size = new Size((int)tempsize, (int)tempsize2);

        }
        //List<Point> pStartList = new List<Point>();
       // List<Point> pEndList = new List<Point>();

        int startX = 0;
        int startY = 0;
        int endX = 0;
        int endY = 0;


        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {    
                if (startX == 0)
                {
                    startX = e.X;
                    startY = e.Y;
                }
                else
                {
                    Graphics grp = pictureBox1.CreateGraphics();
                    endX = e.X;
                    endY = e.Y;

                    if ((startX - endX) < 0 && (startY - endY) < 0)
                    {
                        grp.DrawRectangle(Pens.Red, startX, startY, -(startX - endX), -(startY - endY));
                    }
                    else if((startX - endX) < 0 && (startY - endY)  > 0)
                    {
                        grp.DrawRectangle(Pens.Red, startX, endY, -(startX - endX), (startY - endY));
                    }
                    else if ((startX - endX) > 0 && (startY - endY) < 0)
                    {
                        grp.DrawRectangle(Pens.Red, endX, startY, (startX - endX), -(startY - endY));
                    }
                    else
                    {
                        grp.DrawRectangle(Pens.Red, endX, endY, (startX - endX), (startY - endY));
                    }
                    //pStartList.Add(new Point(startX, startY));
                    //pEndList.Add(new Point(endX, endY));

                    startX = 0;
                    startY = 0;
                }
               
            }        
        }

        private void DrawRectangle()
        {
            /*
            foreach (var pnt in pStartList)
            {
               //int x =  pnt.X;
            }
            */
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            Graphics grp = pictureBox1.CreateGraphics();        

            if (startX != 0)
            {
                Refresh();
                endX = e.X;
                endY = e.Y;

                if ((startX - endX) < 0 && (startY - endY) < 0)
                {
                    grp.DrawRectangle(Pens.Red, startX, startY, -(startX - endX), -(startY - endY));
                }
                else if ((startX - endX) < 0 && (startY - endY) > 0)
                {
                    grp.DrawRectangle(Pens.Red, startX, endY, -(startX - endX), (startY - endY));
                }
                else if ((startX - endX) > 0 && (startY - endY) < 0)
                {
                    grp.DrawRectangle(Pens.Red, endX, startY, (startX - endX), -(startY - endY));
                }
                else
                {
                    grp.DrawRectangle(Pens.Red, endX, endY, (startX - endX), (startY - endY));
                }
               
            }

        }

    }
}
